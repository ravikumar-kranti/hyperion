import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProjectsComponent } from './projects/projects.component';
import { SidebarMenuComponent } from './sidebar-menu/sidebar-menu.component';
import { SidebarSubmenuComponent } from './sidebar-submenu/sidebar-submenu.component';

const routes: Routes = [
  {
    path:'',
    component: HomeComponent
  },
  {
    path:'menu',
    component: SidebarMenuComponent
  },
  {
    path:'menu-with-dropdown',
    component: SidebarSubmenuComponent
  },
  {
    path:'projects',
    component: ProjectsComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
