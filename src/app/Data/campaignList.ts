export const campaignList = [{
        "imageUrl": "./../../assets/hyperion/clipboard/campaign_active@2x.png",
        "imageSrcset": "./../../assets/hyperion/clipboard/campaign_active.png@2x.png 2x, ./../../assets/hyperion/clipboard/campaign_active.png@3x.png 3x",
        "campaignName": "Credit card write off",
        "campaignZone": "North Zone An",
        "campaignRecords": "140 Records",
        "campaignDate": "02-Feb-2021",
        "campaignEngagement": "50% engaged"
    },
    {
        "imageUrl": "./../../assets/hyperion/clipboard/campaign_active@2x.png",
        "imageSrcset": "./../../assets/hyperion/clipboard/campaign_active.png@2x.png 2x, ./../../assets/hyperion/clipboard/campaign_active.png@3x.png 3x",
        "campaignName": "Credit card write off",
        "campaignZone": "North Zone An",
        "campaignRecords": "60 Records",
        "campaignDate": "02-Feb-2021",
        "campaignEngagement": "92% engaged"
    },
    {
        "imageUrl": "./../../assets/hyperion/clipboard/campaign_active@2x.png",
        "imageSrcset": "./../../assets/hyperion/clipboard/campaign_active.png@2x.png 2x, ./../../assets/hyperion/clipboard/campaign_active.png@3x.png 3x",
        "campaignName": "Credit card write off",
        "campaignZone": "South Zone An",
        "campaignRecords": "147 Records",
        "campaignDate": "02-Feb-2021",
        "campaignEngagement": "84% engaged"
    },
    {
        "imageUrl": "./../../assets/hyperion/clipboard/campaign_inactive@2x.png",
        "imageSrcset": "./../../assets/hyperion/clipboard/campaign_active.png@2x.png 2x, ./../../assets/hyperion/clipboard/campaign_active.png@3x.png 3x",
        "campaignName": "Credit card write off",
        "campaignZone": "South Zone An",
        "campaignRecords": "140 Records",
        "campaignDate": "02-Feb-2021",
        "campaignEngagement": "69% engaged"
    }
];