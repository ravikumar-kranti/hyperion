import { Options } from "highcharts";

export const dognutChartOptions: Options = {
    chart: {
        type: 'pie',
        plotShadow: false,
        borderRadius: 0
    },
    credits: {
        enabled: false,
    },
    plotOptions: {
        pie: {
            innerSize: '99%',
            borderWidth: 20,
            borderColor: '',
            slicedOffset: 0,
            dataLabels: {
                connectorWidth: 0,
            }
        }
    },
    title: {
        verticalAlign: 'middle',
        floating: false,
        text: '10,000',
        margin: 10,

    },
    subtitle: {
        align: 'center',
        floating: false,
        style: { "color": "#666666" },
        text: 'Total',
        useHTML: false,
        verticalAlign: undefined,
        x: 0,
        y: 210
    },
    legend: {
        enabled: true,
    },
    series: [
        {
            type: 'pie',
            data: [
                { name: 'a', y: 1, color: '#1d3433' },
                { name: 'b', y: 1, color: '#2d3433' },
                { name: 'c', y: 1, color: '#3d3433' },
                { name: 'd', y: 1, color: '#4d3433' },
                { name: 'e', y: 1, color: '#5d3433' },
            ]
        }
    ],
    pane: {
        background: [
            {
                backgroundColor: "linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 }, stops: [[0, #2a538b], [1, #2a538b]]",
                outerRadius: '0%',
            }
        ]
    }

}