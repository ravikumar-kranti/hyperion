export const DashboardData = {
    name: 'CC Reminders',
    username: 'Rohit',
    userimg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABQVBMVEX////t5/ZnOrf/yih5VUjttAUxG5J4Rxn/zSfu6PdxTUnt6PnVpjXtvSj8+/7y7vn18vrt3Mzz7v7/yAC8rLFzTT3s6Pz59/x1UUnyuABfLLT/0CnywShiMbVkNbZbJLJxPxhxSThtOxjtugDl3vJZIbLv5OP3wBtTOIJfNbEiC5aBX8J8WMDb0dyXfXni2eaxnqHisTCBXEaIYkV9Wk/SozWFZl3FkyG5iSCpeB6CURpsSUqhcB3tvzPtynWPXxvtx2X22aT+zDqbgs5BJJyvm9fUyOm9rN6JasYaAJgmEJU7I4x0U3OpflGRdclsQbnMv8hqPymehYS7qq6kez+whj2+kjqUbUOfd0D41Yn03bigmMyznqpGNZtnPZJHKnyPhL93arJAH6TRmkurenOZcV7Qw+igiNB8WW1rTHe2pNsRhAU5AAALxElEQVR4nO2de1vbRhbGK7k4EsaWMfh+EQY7EDC3ODg0JIRgDNhtY0IKyTabXnY30PL9P8CO5Itk6zZzZjQyffT+kzzI1uin95wzM5I8+u67UKFChQoVKlSoUKFChQoVKlQoYiV0BX0UrJXIZpOKIsuCWbKsKMlk9tHDJpLKJJhVspJ8rJjZpBecCTOZDfpwCYW8w6Yb6RF5mSAwb9rKRwAJx3sckFny4LSBnN2cTDLAGygZNIqdEizsM6TMWrAy5ps5Rh/4ZovRHz6dMWg0Xezqi52CrzlZX/mE4PsO/wLUUJCh6ruBQwVmIw8DBwrGxgQ3Pk0B2OhvCbWKe1HlF6Ej8Y3UBN0UCSiOQxxeNXRa3JKRdwoa4pSM/FPQEJdkDBKQC2IgNYYnYtCAaCj+Twf0GXEWAH0N1GCLjCHfEGcF0DdEakAZKWUSRcz7gkg1ktHRdrevlieUgu/Qh9ENxVgU4QlXezvparWaNuvDNoWLzMeo8PkuwltGdOnvLUrvUJjIfKYB59u+/mBDp6tKYyLjbhF4JIivZeceExOZVhtglUnt7lQd8TRRZSLLagOrMnJqz8U/BiYyrDag5lO7LXcDB5lI1S2yAgR19amrtIeBA10vU0AySkVQjKY+ehs4CNR0tfVRgDKyiVMQ4B4m4ICyer0LTEgWgJAYJQPUGfdgNjKIU0iMYoeombG1DbKRPk4BZzZ1RQ6IVL2CIFIPbQB9vbwNAkSIHyGItP0+AFCA8YER6QABZSa1g9UP2uoDZMpIVWwAcyZgEo5chAxUaeZR5BbKAtxBTS9kckQKEyEW7tERpvcAcQo3EWDhLk2MaqrucjQRYuE1nYXIxGuOJgZhIV8TiRsitjA3P8/GRBgg+XCGrJDm5nO/LMRtNgjkJsIGNsTNCKllbMLc/Pe/LESKkhTPTW+qXgH6RAggYFKRamHj3exLRSmCJL2ZDlTQxRvIFIP8ROLVmdyRgacj7lsQAb0+YIoB6Sq8gzR39Pxmv2jgaSouHE1+CBSm5B0GYNqU2vHEuzW7N0Y8mEQEjWvIaw15G4LsZmHuqHn7BlWWaTw7RNBVVFJAQJ1xmfnOI7y4ZI+nI95OIkIu2ZDWGsjc3iENdTwH98aIN+ZyAxnWEIcpeQu20wo0aPmk4bnRWRFBpYYwTEFX2KYn97n5o08HJQw8HfGzgZhehhCSVVPIzabJ/l5zDxtPk/RpPLoBFVPCMIWcw9QLM95nIjwdsTlCBA2+CTt9QAOCbBj4+SBCiKerOfo+7KYbCSAkDY2riPMLEDxk4sIoFVu+X/6G3KswE4IAqQlJEhGShiwJX4AISRIRtH+WhLDbifiAoIdnWBKChm0kPSLo0QumhIALGQJJIoJu3M8AIf4lN9DuZ4AQv9RA9u5EKLlMmqY2UhNilxrgU3pyK20lLMZfHr4sFW35iiW0LW5sGxOmW8BHM3BLDfBBS1loWQilH1ZXVlZWD+0Qi4f6th8kC2ELaCH2qAb6rKwsDMbOBqF0uPpE06pNZkoLw22H0iRhrgkFxC6mUMLMcVxHNHm48mSgV1YTi6+G21YmPcw148cZnwmBD3THFtfiOuKYUIqvjijWLYTrI/rVuGQiRIDxtcUY7BBwuwtgjGSOS3Ed0Y6wZCEs2RLmmmgnJaiJuN0FlPANAixFmjlTlI4In1ijVBptWjVFaa4Z0U7TG58JYXsXhLhGKEWaR2PC4uuBUSs2xbR4ONz2ujgmPGpGJI0wDjwCnwmHHkqR5wdG5Xy1uvJkZfWtXacvvdW3vTL+cPAcfZvGQ9wuH0qo5SHKN6m0bwJ6+frta4dRnLSAtr00tkn7KAIiNHmISQj94YFeS/WKYgaSikXHaxrT2/T/lyhqqc+EyMS1uLVmkqoUXwNbiDlsg/94JHayxoBw7QTqoP+EQuYdA8J3YAc5EAoZhRZQUigAORAKmVNKwlMaQC6EJ5SEJzNP+G6JCnCJJgu5EMYWKQnBXSEBIXhcqitDRyhRWejzqG1IeExF+C8uhFS/uqcLU8og9Xl+OBRVf0HXV/AipKimlJUU+yoG7Q/vYVe8dVHFKD4h5RpCcBNpLfT9auJIMWgmUmYhPiHtQlfQckpZSAX8a97Uq+lljiGIS5R9oUBwi5S2IUGGxOkprYMEt7mpF9qJPSU3cekpNSH+/UP6hWgyxKm4tEgdowT3gBksOZf5lQxx6Vd6QIL7+CwW7iRDZAJI8lAUg9aIApVFiApED7YxWdMr8/QUj3Hp9CkTQJJnotis/RgTjrGGqCcCfT+hieS5NkbLd8YW8xiAeeqhzFBEz7KzaTK2ODdnvfk7qfW5OVaEJICMFtfTCN0Z17UPMCIke0aYUSLqhM6M64PNjAjJnvNms5T1iFCDnKZcXx9vY0RI+MsnJm2aCIeYQ03+mREhGSCbMJ0mdBAbQtLfzDDpL7gSEv/GkkU15UlI/hNLBmEqpzgSkv/+kLaayopy/uVjHgMw/+9zxfMFdJ4CLDpA06aG98fXzc3fsAh/3/z6By0kZKkheK2RldoXhBeNRp9hEaIP9ja/fqnRMIKWiwIDyn/qeEj/wQCcmxt8eHPzTwpGCCCw1iAD20M+dNT/9TYxf9sdfrpXvocyAheLAvIVymdjwv9hEH7rjT4ulgttGCMMEHDJTZE7hbIoPhsdchSj1OQ3Rh8+E0WxrHZkQLtAQtIOQ1bu+gV0lCbC7r4n4X7XTCiKhf4dsY3gRYbITqYivK9X9IMUx4TeYZpvjYP02eDLlXqbcNkI+EJRRCYq5/2yONQ4EaNRTw+Nj46+jWw8Jzq5FIt9EbSjPIwMnDTxm7uJ+W/jwntmfL1SvyBpGg6Ib6KstFXRJMPE7r4bYt7IwlGQDqTe4ycj1brXmGdSli8L5uMz1ZrehivhRs/GQj1SL3GTkXKBTzxAoVGePD6TiZt/OSPm/9q0t1DrNxqYiHSAWAMbOWYBNGVitNtyQsy3jBg9s+yhvIWFSL3mtXcjsmwDaIrTaPfvOTvG/NzfBmDUugc8RPql2b2LjXJpAzjRY/R+vM1PM+bztz/2jI9Mx+ggF9971wEGayV7NaK0C3ZHZ45TZOPGDWLKj+nyczcbJgNtYnSAeO/ZOj2gV7FRLlT7ozPHKbKxG/3p5wONDdEd/PxTtNszb3bYhah69YssAN2nwnLNwcFpRG2W2+1u9nqDfyY3Oe5CLNRcU5HRuvNup1FuVJwPbwrRUc57ECtbbq0zez2CSxMdZwuxEd32IKoPLoisAJ3jVK7VXQ9voqI6yKHIjOUSpwzfxOLU7yvvXWIUD9G2mzCr3HYykenbdOwbkc+d6qhJz9wYvQzUpJ7bm8j4jUj2J3HL00JXxjNPAzVVHPp9toC2QxssC50Z8fiQVNtMZP5WUptqg5GFZkgz5Rk2HlLZbmTjwztJLdVGruFaOKYcivBrYjlmMdGXN5JOn0iPvpChCpaxG59Xyyluwxmmsgxs+LweEL/O0Guq1/fxLZbmdvgF6XSY8npNp3LJK0inukRer1qVY/wsRJJ5ARq5KN/xS0PzyI3DK4+HiMoDTw/HicjxtdVEAxpqjYY1XACHoxt+vaGmyqVOyOnd6voYVRY48iHCvkbow1jUSQnZ7QqUL4gxH2YTrlI4jmg0qTVOKWioVuCah2qNNyBycYvjuLTB3UFdE3d9fVSl3gmED6nW51FuCv0AInQs/20M0MCBhIa/2ahuBWngQHeif6FaEO+CxtPVUW1vkVKrrHb4dvLOSt7X2TOW6+1gugh7KW3GjIjP96kuoYR2nV0+Fur3s8anKdmpqCz6jooqdrhNkwiVuGvUKYerlUK9MRv100lCpw/PyEq53u+wvqfkg2r3olomd7JSUPud4Lt3TNU6KFwJKCtltb718GjwBlLu7vsF1RuzUkafanTOOV6hYKjk+UW7X1cLiNMKWtHY6vV++6L2OOnGStTuHu4vG32UZ3VVU72OoMV+433n4nwWez2wstmkUkMSBCWZzM7KgDNUqFChQoUKFSpUqFChQoUK9Vj0f0RDrEgIogMPAAAAAElFTkSuQmCC',
    cardData: {
        recordData: {
            name: "Count of Records",
            totalRecords: '1,00,000',
            recordImg: ''
        },
        segmentData: {
            name: 'Number of Segments',
            totalSegments: '150',
            segmentImg: ''
        },
        fileData: {
            name: 'Numbers of Data',
            totalFiles: '05',
            filesImg: ''
        },
    },
    projectTeam: [
        {
            name: 'Yashraj Nagar',
            designation: 'CXO Manager',
            imgUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABQVBMVEX////t5/ZnOrf/yih5VUjttAUxG5J4Rxn/zSfu6PdxTUnt6PnVpjXtvSj8+/7y7vn18vrt3Mzz7v7/yAC8rLFzTT3s6Pz59/x1UUnyuABfLLT/0CnywShiMbVkNbZbJLJxPxhxSThtOxjtugDl3vJZIbLv5OP3wBtTOIJfNbEiC5aBX8J8WMDb0dyXfXni2eaxnqHisTCBXEaIYkV9Wk/SozWFZl3FkyG5iSCpeB6CURpsSUqhcB3tvzPtynWPXxvtx2X22aT+zDqbgs5BJJyvm9fUyOm9rN6JasYaAJgmEJU7I4x0U3OpflGRdclsQbnMv8hqPymehYS7qq6kez+whj2+kjqUbUOfd0D41Yn03bigmMyznqpGNZtnPZJHKnyPhL93arJAH6TRmkurenOZcV7Qw+igiNB8WW1rTHe2pNsRhAU5AAALxElEQVR4nO2de1vbRhbGK7k4EsaWMfh+EQY7EDC3ODg0JIRgDNhtY0IKyTabXnY30PL9P8CO5Itk6zZzZjQyffT+kzzI1uin95wzM5I8+u67UKFChQoVKlSoUKFChQoVKlQoYiV0BX0UrJXIZpOKIsuCWbKsKMlk9tHDJpLKJJhVspJ8rJjZpBecCTOZDfpwCYW8w6Yb6RF5mSAwb9rKRwAJx3sckFny4LSBnN2cTDLAGygZNIqdEizsM6TMWrAy5ps5Rh/4ZovRHz6dMWg0Xezqi52CrzlZX/mE4PsO/wLUUJCh6ruBQwVmIw8DBwrGxgQ3Pk0B2OhvCbWKe1HlF6Ej8Y3UBN0UCSiOQxxeNXRa3JKRdwoa4pSM/FPQEJdkDBKQC2IgNYYnYtCAaCj+Twf0GXEWAH0N1GCLjCHfEGcF0DdEakAZKWUSRcz7gkg1ktHRdrevlieUgu/Qh9ENxVgU4QlXezvparWaNuvDNoWLzMeo8PkuwltGdOnvLUrvUJjIfKYB59u+/mBDp6tKYyLjbhF4JIivZeceExOZVhtglUnt7lQd8TRRZSLLagOrMnJqz8U/BiYyrDag5lO7LXcDB5lI1S2yAgR19amrtIeBA10vU0AySkVQjKY+ehs4CNR0tfVRgDKyiVMQ4B4m4ICyer0LTEgWgJAYJQPUGfdgNjKIU0iMYoeombG1DbKRPk4BZzZ1RQ6IVL2CIFIPbQB9vbwNAkSIHyGItP0+AFCA8YER6QABZSa1g9UP2uoDZMpIVWwAcyZgEo5chAxUaeZR5BbKAtxBTS9kckQKEyEW7tERpvcAcQo3EWDhLk2MaqrucjQRYuE1nYXIxGuOJgZhIV8TiRsitjA3P8/GRBgg+XCGrJDm5nO/LMRtNgjkJsIGNsTNCKllbMLc/Pe/LESKkhTPTW+qXgH6RAggYFKRamHj3exLRSmCJL2ZDlTQxRvIFIP8ROLVmdyRgacj7lsQAb0+YIoB6Sq8gzR39Pxmv2jgaSouHE1+CBSm5B0GYNqU2vHEuzW7N0Y8mEQEjWvIaw15G4LsZmHuqHn7BlWWaTw7RNBVVFJAQJ1xmfnOI7y4ZI+nI95OIkIu2ZDWGsjc3iENdTwH98aIN+ZyAxnWEIcpeQu20wo0aPmk4bnRWRFBpYYwTEFX2KYn97n5o08HJQw8HfGzgZhehhCSVVPIzabJ/l5zDxtPk/RpPLoBFVPCMIWcw9QLM95nIjwdsTlCBA2+CTt9QAOCbBj4+SBCiKerOfo+7KYbCSAkDY2riPMLEDxk4sIoFVu+X/6G3KswE4IAqQlJEhGShiwJX4AISRIRtH+WhLDbifiAoIdnWBKChm0kPSLo0QumhIALGQJJIoJu3M8AIf4lN9DuZ4AQv9RA9u5EKLlMmqY2UhNilxrgU3pyK20lLMZfHr4sFW35iiW0LW5sGxOmW8BHM3BLDfBBS1loWQilH1ZXVlZWD+0Qi4f6th8kC2ELaCH2qAb6rKwsDMbOBqF0uPpE06pNZkoLw22H0iRhrgkFxC6mUMLMcVxHNHm48mSgV1YTi6+G21YmPcw148cZnwmBD3THFtfiOuKYUIqvjijWLYTrI/rVuGQiRIDxtcUY7BBwuwtgjGSOS3Ed0Y6wZCEs2RLmmmgnJaiJuN0FlPANAixFmjlTlI4In1ijVBptWjVFaa4Z0U7TG58JYXsXhLhGKEWaR2PC4uuBUSs2xbR4ONz2ujgmPGpGJI0wDjwCnwmHHkqR5wdG5Xy1uvJkZfWtXacvvdW3vTL+cPAcfZvGQ9wuH0qo5SHKN6m0bwJ6+frta4dRnLSAtr00tkn7KAIiNHmISQj94YFeS/WKYgaSikXHaxrT2/T/lyhqqc+EyMS1uLVmkqoUXwNbiDlsg/94JHayxoBw7QTqoP+EQuYdA8J3YAc5EAoZhRZQUigAORAKmVNKwlMaQC6EJ5SEJzNP+G6JCnCJJgu5EMYWKQnBXSEBIXhcqitDRyhRWejzqG1IeExF+C8uhFS/uqcLU8og9Xl+OBRVf0HXV/AipKimlJUU+yoG7Q/vYVe8dVHFKD4h5RpCcBNpLfT9auJIMWgmUmYhPiHtQlfQckpZSAX8a97Uq+lljiGIS5R9oUBwi5S2IUGGxOkprYMEt7mpF9qJPSU3cekpNSH+/UP6hWgyxKm4tEgdowT3gBksOZf5lQxx6Vd6QIL7+CwW7iRDZAJI8lAUg9aIApVFiApED7YxWdMr8/QUj3Hp9CkTQJJnotis/RgTjrGGqCcCfT+hieS5NkbLd8YW8xiAeeqhzFBEz7KzaTK2ODdnvfk7qfW5OVaEJICMFtfTCN0Z17UPMCIke0aYUSLqhM6M64PNjAjJnvNms5T1iFCDnKZcXx9vY0RI+MsnJm2aCIeYQ03+mREhGSCbMJ0mdBAbQtLfzDDpL7gSEv/GkkU15UlI/hNLBmEqpzgSkv/+kLaayopy/uVjHgMw/+9zxfMFdJ4CLDpA06aG98fXzc3fsAh/3/z6By0kZKkheK2RldoXhBeNRp9hEaIP9ja/fqnRMIKWiwIDyn/qeEj/wQCcmxt8eHPzTwpGCCCw1iAD20M+dNT/9TYxf9sdfrpXvocyAheLAvIVymdjwv9hEH7rjT4ulgttGCMMEHDJTZE7hbIoPhsdchSj1OQ3Rh8+E0WxrHZkQLtAQtIOQ1bu+gV0lCbC7r4n4X7XTCiKhf4dsY3gRYbITqYivK9X9IMUx4TeYZpvjYP02eDLlXqbcNkI+EJRRCYq5/2yONQ4EaNRTw+Nj46+jWw8Jzq5FIt9EbSjPIwMnDTxm7uJ+W/jwntmfL1SvyBpGg6Ib6KstFXRJMPE7r4bYt7IwlGQDqTe4ycj1brXmGdSli8L5uMz1ZrehivhRs/GQj1SL3GTkXKBTzxAoVGePD6TiZt/OSPm/9q0t1DrNxqYiHSAWAMbOWYBNGVitNtyQsy3jBg9s+yhvIWFSL3mtXcjsmwDaIrTaPfvOTvG/NzfBmDUugc8RPql2b2LjXJpAzjRY/R+vM1PM+bztz/2jI9Mx+ggF9971wEGayV7NaK0C3ZHZ45TZOPGDWLKj+nyczcbJgNtYnSAeO/ZOj2gV7FRLlT7ozPHKbKxG/3p5wONDdEd/PxTtNszb3bYhah69YssAN2nwnLNwcFpRG2W2+1u9nqDfyY3Oe5CLNRcU5HRuvNup1FuVJwPbwrRUc57ECtbbq0zez2CSxMdZwuxEd32IKoPLoisAJ3jVK7VXQ9voqI6yKHIjOUSpwzfxOLU7yvvXWIUD9G2mzCr3HYykenbdOwbkc+d6qhJz9wYvQzUpJ7bm8j4jUj2J3HL00JXxjNPAzVVHPp9toC2QxssC50Z8fiQVNtMZP5WUptqg5GFZkgz5Rk2HlLZbmTjwztJLdVGruFaOKYcivBrYjlmMdGXN5JOn0iPvpChCpaxG59Xyyluwxmmsgxs+LweEL/O0Guq1/fxLZbmdvgF6XSY8npNp3LJK0inukRer1qVY/wsRJJ5ARq5KN/xS0PzyI3DK4+HiMoDTw/HicjxtdVEAxpqjYY1XACHoxt+vaGmyqVOyOnd6voYVRY48iHCvkbow1jUSQnZ7QqUL4gxH2YTrlI4jmg0qTVOKWioVuCah2qNNyBycYvjuLTB3UFdE3d9fVSl3gmED6nW51FuCv0AInQs/20M0MCBhIa/2ahuBWngQHeif6FaEO+CxtPVUW1vkVKrrHb4dvLOSt7X2TOW6+1gugh7KW3GjIjP96kuoYR2nV0+Fur3s8anKdmpqCz6jooqdrhNkwiVuGvUKYerlUK9MRv100lCpw/PyEq53u+wvqfkg2r3olomd7JSUPud4Lt3TNU6KFwJKCtltb718GjwBlLu7vsF1RuzUkafanTOOV6hYKjk+UW7X1cLiNMKWtHY6vV++6L2OOnGStTuHu4vG32UZ3VVU72OoMV+433n4nwWez2wstmkUkMSBCWZzM7KgDNUqFChQoUKFSpUqFChQoUK9Vj0f0RDrEgIogMPAAAAAElFTkSuQmCC",
        },
        {
            name: 'Damini Nagar',
            designation: 'Senior Manager',
            imgUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABQVBMVEX////t5/ZnOrf/yih5VUjttAUxG5J4Rxn/zSfu6PdxTUnt6PnVpjXtvSj8+/7y7vn18vrt3Mzz7v7/yAC8rLFzTT3s6Pz59/x1UUnyuABfLLT/0CnywShiMbVkNbZbJLJxPxhxSThtOxjtugDl3vJZIbLv5OP3wBtTOIJfNbEiC5aBX8J8WMDb0dyXfXni2eaxnqHisTCBXEaIYkV9Wk/SozWFZl3FkyG5iSCpeB6CURpsSUqhcB3tvzPtynWPXxvtx2X22aT+zDqbgs5BJJyvm9fUyOm9rN6JasYaAJgmEJU7I4x0U3OpflGRdclsQbnMv8hqPymehYS7qq6kez+whj2+kjqUbUOfd0D41Yn03bigmMyznqpGNZtnPZJHKnyPhL93arJAH6TRmkurenOZcV7Qw+igiNB8WW1rTHe2pNsRhAU5AAALxElEQVR4nO2de1vbRhbGK7k4EsaWMfh+EQY7EDC3ODg0JIRgDNhtY0IKyTabXnY30PL9P8CO5Itk6zZzZjQyffT+kzzI1uin95wzM5I8+u67UKFChQoVKlSoUKFChQoVKlQoYiV0BX0UrJXIZpOKIsuCWbKsKMlk9tHDJpLKJJhVspJ8rJjZpBecCTOZDfpwCYW8w6Yb6RF5mSAwb9rKRwAJx3sckFny4LSBnN2cTDLAGygZNIqdEizsM6TMWrAy5ps5Rh/4ZovRHz6dMWg0Xezqi52CrzlZX/mE4PsO/wLUUJCh6ruBQwVmIw8DBwrGxgQ3Pk0B2OhvCbWKe1HlF6Ej8Y3UBN0UCSiOQxxeNXRa3JKRdwoa4pSM/FPQEJdkDBKQC2IgNYYnYtCAaCj+Twf0GXEWAH0N1GCLjCHfEGcF0DdEakAZKWUSRcz7gkg1ktHRdrevlieUgu/Qh9ENxVgU4QlXezvparWaNuvDNoWLzMeo8PkuwltGdOnvLUrvUJjIfKYB59u+/mBDp6tKYyLjbhF4JIivZeceExOZVhtglUnt7lQd8TRRZSLLagOrMnJqz8U/BiYyrDag5lO7LXcDB5lI1S2yAgR19amrtIeBA10vU0AySkVQjKY+ehs4CNR0tfVRgDKyiVMQ4B4m4ICyer0LTEgWgJAYJQPUGfdgNjKIU0iMYoeombG1DbKRPk4BZzZ1RQ6IVL2CIFIPbQB9vbwNAkSIHyGItP0+AFCA8YER6QABZSa1g9UP2uoDZMpIVWwAcyZgEo5chAxUaeZR5BbKAtxBTS9kckQKEyEW7tERpvcAcQo3EWDhLk2MaqrucjQRYuE1nYXIxGuOJgZhIV8TiRsitjA3P8/GRBgg+XCGrJDm5nO/LMRtNgjkJsIGNsTNCKllbMLc/Pe/LESKkhTPTW+qXgH6RAggYFKRamHj3exLRSmCJL2ZDlTQxRvIFIP8ROLVmdyRgacj7lsQAb0+YIoB6Sq8gzR39Pxmv2jgaSouHE1+CBSm5B0GYNqU2vHEuzW7N0Y8mEQEjWvIaw15G4LsZmHuqHn7BlWWaTw7RNBVVFJAQJ1xmfnOI7y4ZI+nI95OIkIu2ZDWGsjc3iENdTwH98aIN+ZyAxnWEIcpeQu20wo0aPmk4bnRWRFBpYYwTEFX2KYn97n5o08HJQw8HfGzgZhehhCSVVPIzabJ/l5zDxtPk/RpPLoBFVPCMIWcw9QLM95nIjwdsTlCBA2+CTt9QAOCbBj4+SBCiKerOfo+7KYbCSAkDY2riPMLEDxk4sIoFVu+X/6G3KswE4IAqQlJEhGShiwJX4AISRIRtH+WhLDbifiAoIdnWBKChm0kPSLo0QumhIALGQJJIoJu3M8AIf4lN9DuZ4AQv9RA9u5EKLlMmqY2UhNilxrgU3pyK20lLMZfHr4sFW35iiW0LW5sGxOmW8BHM3BLDfBBS1loWQilH1ZXVlZWD+0Qi4f6th8kC2ELaCH2qAb6rKwsDMbOBqF0uPpE06pNZkoLw22H0iRhrgkFxC6mUMLMcVxHNHm48mSgV1YTi6+G21YmPcw148cZnwmBD3THFtfiOuKYUIqvjijWLYTrI/rVuGQiRIDxtcUY7BBwuwtgjGSOS3Ed0Y6wZCEs2RLmmmgnJaiJuN0FlPANAixFmjlTlI4In1ijVBptWjVFaa4Z0U7TG58JYXsXhLhGKEWaR2PC4uuBUSs2xbR4ONz2ujgmPGpGJI0wDjwCnwmHHkqR5wdG5Xy1uvJkZfWtXacvvdW3vTL+cPAcfZvGQ9wuH0qo5SHKN6m0bwJ6+frta4dRnLSAtr00tkn7KAIiNHmISQj94YFeS/WKYgaSikXHaxrT2/T/lyhqqc+EyMS1uLVmkqoUXwNbiDlsg/94JHayxoBw7QTqoP+EQuYdA8J3YAc5EAoZhRZQUigAORAKmVNKwlMaQC6EJ5SEJzNP+G6JCnCJJgu5EMYWKQnBXSEBIXhcqitDRyhRWejzqG1IeExF+C8uhFS/uqcLU8og9Xl+OBRVf0HXV/AipKimlJUU+yoG7Q/vYVe8dVHFKD4h5RpCcBNpLfT9auJIMWgmUmYhPiHtQlfQckpZSAX8a97Uq+lljiGIS5R9oUBwi5S2IUGGxOkprYMEt7mpF9qJPSU3cekpNSH+/UP6hWgyxKm4tEgdowT3gBksOZf5lQxx6Vd6QIL7+CwW7iRDZAJI8lAUg9aIApVFiApED7YxWdMr8/QUj3Hp9CkTQJJnotis/RgTjrGGqCcCfT+hieS5NkbLd8YW8xiAeeqhzFBEz7KzaTK2ODdnvfk7qfW5OVaEJICMFtfTCN0Z17UPMCIke0aYUSLqhM6M64PNjAjJnvNms5T1iFCDnKZcXx9vY0RI+MsnJm2aCIeYQ03+mREhGSCbMJ0mdBAbQtLfzDDpL7gSEv/GkkU15UlI/hNLBmEqpzgSkv/+kLaayopy/uVjHgMw/+9zxfMFdJ4CLDpA06aG98fXzc3fsAh/3/z6By0kZKkheK2RldoXhBeNRp9hEaIP9ja/fqnRMIKWiwIDyn/qeEj/wQCcmxt8eHPzTwpGCCCw1iAD20M+dNT/9TYxf9sdfrpXvocyAheLAvIVymdjwv9hEH7rjT4ulgttGCMMEHDJTZE7hbIoPhsdchSj1OQ3Rh8+E0WxrHZkQLtAQtIOQ1bu+gV0lCbC7r4n4X7XTCiKhf4dsY3gRYbITqYivK9X9IMUx4TeYZpvjYP02eDLlXqbcNkI+EJRRCYq5/2yONQ4EaNRTw+Nj46+jWw8Jzq5FIt9EbSjPIwMnDTxm7uJ+W/jwntmfL1SvyBpGg6Ib6KstFXRJMPE7r4bYt7IwlGQDqTe4ycj1brXmGdSli8L5uMz1ZrehivhRs/GQj1SL3GTkXKBTzxAoVGePD6TiZt/OSPm/9q0t1DrNxqYiHSAWAMbOWYBNGVitNtyQsy3jBg9s+yhvIWFSL3mtXcjsmwDaIrTaPfvOTvG/NzfBmDUugc8RPql2b2LjXJpAzjRY/R+vM1PM+bztz/2jI9Mx+ggF9971wEGayV7NaK0C3ZHZ45TZOPGDWLKj+nyczcbJgNtYnSAeO/ZOj2gV7FRLlT7ozPHKbKxG/3p5wONDdEd/PxTtNszb3bYhah69YssAN2nwnLNwcFpRG2W2+1u9nqDfyY3Oe5CLNRcU5HRuvNup1FuVJwPbwrRUc57ECtbbq0zez2CSxMdZwuxEd32IKoPLoisAJ3jVK7VXQ9voqI6yKHIjOUSpwzfxOLU7yvvXWIUD9G2mzCr3HYykenbdOwbkc+d6qhJz9wYvQzUpJ7bm8j4jUj2J3HL00JXxjNPAzVVHPp9toC2QxssC50Z8fiQVNtMZP5WUptqg5GFZkgz5Rk2HlLZbmTjwztJLdVGruFaOKYcivBrYjlmMdGXN5JOn0iPvpChCpaxG59Xyyluwxmmsgxs+LweEL/O0Guq1/fxLZbmdvgF6XSY8npNp3LJK0inukRer1qVY/wsRJJ5ARq5KN/xS0PzyI3DK4+HiMoDTw/HicjxtdVEAxpqjYY1XACHoxt+vaGmyqVOyOnd6voYVRY48iHCvkbow1jUSQnZ7QqUL4gxH2YTrlI4jmg0qTVOKWioVuCah2qNNyBycYvjuLTB3UFdE3d9fVSl3gmED6nW51FuCv0AInQs/20M0MCBhIa/2ahuBWngQHeif6FaEO+CxtPVUW1vkVKrrHb4dvLOSt7X2TOW6+1gugh7KW3GjIjP96kuoYR2nV0+Fur3s8anKdmpqCz6jooqdrhNkwiVuGvUKYerlUK9MRv100lCpw/PyEq53u+wvqfkg2r3olomd7JSUPud4Lt3TNU6KFwJKCtltb718GjwBlLu7vsF1RuzUkafanTOOV6hYKjk+UW7X1cLiNMKWtHY6vV++6L2OOnGStTuHu4vG32UZ3VVU72OoMV+433n4nwWez2wstmkUkMSBCWZzM7KgDNUqFChQoUKFSpUqFChQoUK9Vj0f0RDrEgIogMPAAAAAElFTkSuQmCC",
        },
        {
            name: 'Yashraj Nagar',
            designation: 'CXO Manager',
            imgUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABQVBMVEX////t5/ZnOrf/yih5VUjttAUxG5J4Rxn/zSfu6PdxTUnt6PnVpjXtvSj8+/7y7vn18vrt3Mzz7v7/yAC8rLFzTT3s6Pz59/x1UUnyuABfLLT/0CnywShiMbVkNbZbJLJxPxhxSThtOxjtugDl3vJZIbLv5OP3wBtTOIJfNbEiC5aBX8J8WMDb0dyXfXni2eaxnqHisTCBXEaIYkV9Wk/SozWFZl3FkyG5iSCpeB6CURpsSUqhcB3tvzPtynWPXxvtx2X22aT+zDqbgs5BJJyvm9fUyOm9rN6JasYaAJgmEJU7I4x0U3OpflGRdclsQbnMv8hqPymehYS7qq6kez+whj2+kjqUbUOfd0D41Yn03bigmMyznqpGNZtnPZJHKnyPhL93arJAH6TRmkurenOZcV7Qw+igiNB8WW1rTHe2pNsRhAU5AAALxElEQVR4nO2de1vbRhbGK7k4EsaWMfh+EQY7EDC3ODg0JIRgDNhtY0IKyTabXnY30PL9P8CO5Itk6zZzZjQyffT+kzzI1uin95wzM5I8+u67UKFChQoVKlSoUKFChQoVKlQoYiV0BX0UrJXIZpOKIsuCWbKsKMlk9tHDJpLKJJhVspJ8rJjZpBecCTOZDfpwCYW8w6Yb6RF5mSAwb9rKRwAJx3sckFny4LSBnN2cTDLAGygZNIqdEizsM6TMWrAy5ps5Rh/4ZovRHz6dMWg0Xezqi52CrzlZX/mE4PsO/wLUUJCh6ruBQwVmIw8DBwrGxgQ3Pk0B2OhvCbWKe1HlF6Ej8Y3UBN0UCSiOQxxeNXRa3JKRdwoa4pSM/FPQEJdkDBKQC2IgNYYnYtCAaCj+Twf0GXEWAH0N1GCLjCHfEGcF0DdEakAZKWUSRcz7gkg1ktHRdrevlieUgu/Qh9ENxVgU4QlXezvparWaNuvDNoWLzMeo8PkuwltGdOnvLUrvUJjIfKYB59u+/mBDp6tKYyLjbhF4JIivZeceExOZVhtglUnt7lQd8TRRZSLLagOrMnJqz8U/BiYyrDag5lO7LXcDB5lI1S2yAgR19amrtIeBA10vU0AySkVQjKY+ehs4CNR0tfVRgDKyiVMQ4B4m4ICyer0LTEgWgJAYJQPUGfdgNjKIU0iMYoeombG1DbKRPk4BZzZ1RQ6IVL2CIFIPbQB9vbwNAkSIHyGItP0+AFCA8YER6QABZSa1g9UP2uoDZMpIVWwAcyZgEo5chAxUaeZR5BbKAtxBTS9kckQKEyEW7tERpvcAcQo3EWDhLk2MaqrucjQRYuE1nYXIxGuOJgZhIV8TiRsitjA3P8/GRBgg+XCGrJDm5nO/LMRtNgjkJsIGNsTNCKllbMLc/Pe/LESKkhTPTW+qXgH6RAggYFKRamHj3exLRSmCJL2ZDlTQxRvIFIP8ROLVmdyRgacj7lsQAb0+YIoB6Sq8gzR39Pxmv2jgaSouHE1+CBSm5B0GYNqU2vHEuzW7N0Y8mEQEjWvIaw15G4LsZmHuqHn7BlWWaTw7RNBVVFJAQJ1xmfnOI7y4ZI+nI95OIkIu2ZDWGsjc3iENdTwH98aIN+ZyAxnWEIcpeQu20wo0aPmk4bnRWRFBpYYwTEFX2KYn97n5o08HJQw8HfGzgZhehhCSVVPIzabJ/l5zDxtPk/RpPLoBFVPCMIWcw9QLM95nIjwdsTlCBA2+CTt9QAOCbBj4+SBCiKerOfo+7KYbCSAkDY2riPMLEDxk4sIoFVu+X/6G3KswE4IAqQlJEhGShiwJX4AISRIRtH+WhLDbifiAoIdnWBKChm0kPSLo0QumhIALGQJJIoJu3M8AIf4lN9DuZ4AQv9RA9u5EKLlMmqY2UhNilxrgU3pyK20lLMZfHr4sFW35iiW0LW5sGxOmW8BHM3BLDfBBS1loWQilH1ZXVlZWD+0Qi4f6th8kC2ELaCH2qAb6rKwsDMbOBqF0uPpE06pNZkoLw22H0iRhrgkFxC6mUMLMcVxHNHm48mSgV1YTi6+G21YmPcw148cZnwmBD3THFtfiOuKYUIqvjijWLYTrI/rVuGQiRIDxtcUY7BBwuwtgjGSOS3Ed0Y6wZCEs2RLmmmgnJaiJuN0FlPANAixFmjlTlI4In1ijVBptWjVFaa4Z0U7TG58JYXsXhLhGKEWaR2PC4uuBUSs2xbR4ONz2ujgmPGpGJI0wDjwCnwmHHkqR5wdG5Xy1uvJkZfWtXacvvdW3vTL+cPAcfZvGQ9wuH0qo5SHKN6m0bwJ6+frta4dRnLSAtr00tkn7KAIiNHmISQj94YFeS/WKYgaSikXHaxrT2/T/lyhqqc+EyMS1uLVmkqoUXwNbiDlsg/94JHayxoBw7QTqoP+EQuYdA8J3YAc5EAoZhRZQUigAORAKmVNKwlMaQC6EJ5SEJzNP+G6JCnCJJgu5EMYWKQnBXSEBIXhcqitDRyhRWejzqG1IeExF+C8uhFS/uqcLU8og9Xl+OBRVf0HXV/AipKimlJUU+yoG7Q/vYVe8dVHFKD4h5RpCcBNpLfT9auJIMWgmUmYhPiHtQlfQckpZSAX8a97Uq+lljiGIS5R9oUBwi5S2IUGGxOkprYMEt7mpF9qJPSU3cekpNSH+/UP6hWgyxKm4tEgdowT3gBksOZf5lQxx6Vd6QIL7+CwW7iRDZAJI8lAUg9aIApVFiApED7YxWdMr8/QUj3Hp9CkTQJJnotis/RgTjrGGqCcCfT+hieS5NkbLd8YW8xiAeeqhzFBEz7KzaTK2ODdnvfk7qfW5OVaEJICMFtfTCN0Z17UPMCIke0aYUSLqhM6M64PNjAjJnvNms5T1iFCDnKZcXx9vY0RI+MsnJm2aCIeYQ03+mREhGSCbMJ0mdBAbQtLfzDDpL7gSEv/GkkU15UlI/hNLBmEqpzgSkv/+kLaayopy/uVjHgMw/+9zxfMFdJ4CLDpA06aG98fXzc3fsAh/3/z6By0kZKkheK2RldoXhBeNRp9hEaIP9ja/fqnRMIKWiwIDyn/qeEj/wQCcmxt8eHPzTwpGCCCw1iAD20M+dNT/9TYxf9sdfrpXvocyAheLAvIVymdjwv9hEH7rjT4ulgttGCMMEHDJTZE7hbIoPhsdchSj1OQ3Rh8+E0WxrHZkQLtAQtIOQ1bu+gV0lCbC7r4n4X7XTCiKhf4dsY3gRYbITqYivK9X9IMUx4TeYZpvjYP02eDLlXqbcNkI+EJRRCYq5/2yONQ4EaNRTw+Nj46+jWw8Jzq5FIt9EbSjPIwMnDTxm7uJ+W/jwntmfL1SvyBpGg6Ib6KstFXRJMPE7r4bYt7IwlGQDqTe4ycj1brXmGdSli8L5uMz1ZrehivhRs/GQj1SL3GTkXKBTzxAoVGePD6TiZt/OSPm/9q0t1DrNxqYiHSAWAMbOWYBNGVitNtyQsy3jBg9s+yhvIWFSL3mtXcjsmwDaIrTaPfvOTvG/NzfBmDUugc8RPql2b2LjXJpAzjRY/R+vM1PM+bztz/2jI9Mx+ggF9971wEGayV7NaK0C3ZHZ45TZOPGDWLKj+nyczcbJgNtYnSAeO/ZOj2gV7FRLlT7ozPHKbKxG/3p5wONDdEd/PxTtNszb3bYhah69YssAN2nwnLNwcFpRG2W2+1u9nqDfyY3Oe5CLNRcU5HRuvNup1FuVJwPbwrRUc57ECtbbq0zez2CSxMdZwuxEd32IKoPLoisAJ3jVK7VXQ9voqI6yKHIjOUSpwzfxOLU7yvvXWIUD9G2mzCr3HYykenbdOwbkc+d6qhJz9wYvQzUpJ7bm8j4jUj2J3HL00JXxjNPAzVVHPp9toC2QxssC50Z8fiQVNtMZP5WUptqg5GFZkgz5Rk2HlLZbmTjwztJLdVGruFaOKYcivBrYjlmMdGXN5JOn0iPvpChCpaxG59Xyyluwxmmsgxs+LweEL/O0Guq1/fxLZbmdvgF6XSY8npNp3LJK0inukRer1qVY/wsRJJ5ARq5KN/xS0PzyI3DK4+HiMoDTw/HicjxtdVEAxpqjYY1XACHoxt+vaGmyqVOyOnd6voYVRY48iHCvkbow1jUSQnZ7QqUL4gxH2YTrlI4jmg0qTVOKWioVuCah2qNNyBycYvjuLTB3UFdE3d9fVSl3gmED6nW51FuCv0AInQs/20M0MCBhIa/2ahuBWngQHeif6FaEO+CxtPVUW1vkVKrrHb4dvLOSt7X2TOW6+1gugh7KW3GjIjP96kuoYR2nV0+Fur3s8anKdmpqCz6jooqdrhNkwiVuGvUKYerlUK9MRv100lCpw/PyEq53u+wvqfkg2r3olomd7JSUPud4Lt3TNU6KFwJKCtltb718GjwBlLu7vsF1RuzUkafanTOOV6hYKjk+UW7X1cLiNMKWtHY6vV++6L2OOnGStTuHu4vG32UZ3VVU72OoMV+433n4nwWez2wstmkUkMSBCWZzM7KgDNUqFChQoUKFSpUqFChQoUK9Vj0f0RDrEgIogMPAAAAAElFTkSuQmCC",
        },
        {
            name: 'Damini Nagar',
            designation: 'Senior Manager',
            imgUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABQVBMVEX////t5/ZnOrf/yih5VUjttAUxG5J4Rxn/zSfu6PdxTUnt6PnVpjXtvSj8+/7y7vn18vrt3Mzz7v7/yAC8rLFzTT3s6Pz59/x1UUnyuABfLLT/0CnywShiMbVkNbZbJLJxPxhxSThtOxjtugDl3vJZIbLv5OP3wBtTOIJfNbEiC5aBX8J8WMDb0dyXfXni2eaxnqHisTCBXEaIYkV9Wk/SozWFZl3FkyG5iSCpeB6CURpsSUqhcB3tvzPtynWPXxvtx2X22aT+zDqbgs5BJJyvm9fUyOm9rN6JasYaAJgmEJU7I4x0U3OpflGRdclsQbnMv8hqPymehYS7qq6kez+whj2+kjqUbUOfd0D41Yn03bigmMyznqpGNZtnPZJHKnyPhL93arJAH6TRmkurenOZcV7Qw+igiNB8WW1rTHe2pNsRhAU5AAALxElEQVR4nO2de1vbRhbGK7k4EsaWMfh+EQY7EDC3ODg0JIRgDNhtY0IKyTabXnY30PL9P8CO5Itk6zZzZjQyffT+kzzI1uin95wzM5I8+u67UKFChQoVKlSoUKFChQoVKlQoYiV0BX0UrJXIZpOKIsuCWbKsKMlk9tHDJpLKJJhVspJ8rJjZpBecCTOZDfpwCYW8w6Yb6RF5mSAwb9rKRwAJx3sckFny4LSBnN2cTDLAGygZNIqdEizsM6TMWrAy5ps5Rh/4ZovRHz6dMWg0Xezqi52CrzlZX/mE4PsO/wLUUJCh6ruBQwVmIw8DBwrGxgQ3Pk0B2OhvCbWKe1HlF6Ej8Y3UBN0UCSiOQxxeNXRa3JKRdwoa4pSM/FPQEJdkDBKQC2IgNYYnYtCAaCj+Twf0GXEWAH0N1GCLjCHfEGcF0DdEakAZKWUSRcz7gkg1ktHRdrevlieUgu/Qh9ENxVgU4QlXezvparWaNuvDNoWLzMeo8PkuwltGdOnvLUrvUJjIfKYB59u+/mBDp6tKYyLjbhF4JIivZeceExOZVhtglUnt7lQd8TRRZSLLagOrMnJqz8U/BiYyrDag5lO7LXcDB5lI1S2yAgR19amrtIeBA10vU0AySkVQjKY+ehs4CNR0tfVRgDKyiVMQ4B4m4ICyer0LTEgWgJAYJQPUGfdgNjKIU0iMYoeombG1DbKRPk4BZzZ1RQ6IVL2CIFIPbQB9vbwNAkSIHyGItP0+AFCA8YER6QABZSa1g9UP2uoDZMpIVWwAcyZgEo5chAxUaeZR5BbKAtxBTS9kckQKEyEW7tERpvcAcQo3EWDhLk2MaqrucjQRYuE1nYXIxGuOJgZhIV8TiRsitjA3P8/GRBgg+XCGrJDm5nO/LMRtNgjkJsIGNsTNCKllbMLc/Pe/LESKkhTPTW+qXgH6RAggYFKRamHj3exLRSmCJL2ZDlTQxRvIFIP8ROLVmdyRgacj7lsQAb0+YIoB6Sq8gzR39Pxmv2jgaSouHE1+CBSm5B0GYNqU2vHEuzW7N0Y8mEQEjWvIaw15G4LsZmHuqHn7BlWWaTw7RNBVVFJAQJ1xmfnOI7y4ZI+nI95OIkIu2ZDWGsjc3iENdTwH98aIN+ZyAxnWEIcpeQu20wo0aPmk4bnRWRFBpYYwTEFX2KYn97n5o08HJQw8HfGzgZhehhCSVVPIzabJ/l5zDxtPk/RpPLoBFVPCMIWcw9QLM95nIjwdsTlCBA2+CTt9QAOCbBj4+SBCiKerOfo+7KYbCSAkDY2riPMLEDxk4sIoFVu+X/6G3KswE4IAqQlJEhGShiwJX4AISRIRtH+WhLDbifiAoIdnWBKChm0kPSLo0QumhIALGQJJIoJu3M8AIf4lN9DuZ4AQv9RA9u5EKLlMmqY2UhNilxrgU3pyK20lLMZfHr4sFW35iiW0LW5sGxOmW8BHM3BLDfBBS1loWQilH1ZXVlZWD+0Qi4f6th8kC2ELaCH2qAb6rKwsDMbOBqF0uPpE06pNZkoLw22H0iRhrgkFxC6mUMLMcVxHNHm48mSgV1YTi6+G21YmPcw148cZnwmBD3THFtfiOuKYUIqvjijWLYTrI/rVuGQiRIDxtcUY7BBwuwtgjGSOS3Ed0Y6wZCEs2RLmmmgnJaiJuN0FlPANAixFmjlTlI4In1ijVBptWjVFaa4Z0U7TG58JYXsXhLhGKEWaR2PC4uuBUSs2xbR4ONz2ujgmPGpGJI0wDjwCnwmHHkqR5wdG5Xy1uvJkZfWtXacvvdW3vTL+cPAcfZvGQ9wuH0qo5SHKN6m0bwJ6+frta4dRnLSAtr00tkn7KAIiNHmISQj94YFeS/WKYgaSikXHaxrT2/T/lyhqqc+EyMS1uLVmkqoUXwNbiDlsg/94JHayxoBw7QTqoP+EQuYdA8J3YAc5EAoZhRZQUigAORAKmVNKwlMaQC6EJ5SEJzNP+G6JCnCJJgu5EMYWKQnBXSEBIXhcqitDRyhRWejzqG1IeExF+C8uhFS/uqcLU8og9Xl+OBRVf0HXV/AipKimlJUU+yoG7Q/vYVe8dVHFKD4h5RpCcBNpLfT9auJIMWgmUmYhPiHtQlfQckpZSAX8a97Uq+lljiGIS5R9oUBwi5S2IUGGxOkprYMEt7mpF9qJPSU3cekpNSH+/UP6hWgyxKm4tEgdowT3gBksOZf5lQxx6Vd6QIL7+CwW7iRDZAJI8lAUg9aIApVFiApED7YxWdMr8/QUj3Hp9CkTQJJnotis/RgTjrGGqCcCfT+hieS5NkbLd8YW8xiAeeqhzFBEz7KzaTK2ODdnvfk7qfW5OVaEJICMFtfTCN0Z17UPMCIke0aYUSLqhM6M64PNjAjJnvNms5T1iFCDnKZcXx9vY0RI+MsnJm2aCIeYQ03+mREhGSCbMJ0mdBAbQtLfzDDpL7gSEv/GkkU15UlI/hNLBmEqpzgSkv/+kLaayopy/uVjHgMw/+9zxfMFdJ4CLDpA06aG98fXzc3fsAh/3/z6By0kZKkheK2RldoXhBeNRp9hEaIP9ja/fqnRMIKWiwIDyn/qeEj/wQCcmxt8eHPzTwpGCCCw1iAD20M+dNT/9TYxf9sdfrpXvocyAheLAvIVymdjwv9hEH7rjT4ulgttGCMMEHDJTZE7hbIoPhsdchSj1OQ3Rh8+E0WxrHZkQLtAQtIOQ1bu+gV0lCbC7r4n4X7XTCiKhf4dsY3gRYbITqYivK9X9IMUx4TeYZpvjYP02eDLlXqbcNkI+EJRRCYq5/2yONQ4EaNRTw+Nj46+jWw8Jzq5FIt9EbSjPIwMnDTxm7uJ+W/jwntmfL1SvyBpGg6Ib6KstFXRJMPE7r4bYt7IwlGQDqTe4ycj1brXmGdSli8L5uMz1ZrehivhRs/GQj1SL3GTkXKBTzxAoVGePD6TiZt/OSPm/9q0t1DrNxqYiHSAWAMbOWYBNGVitNtyQsy3jBg9s+yhvIWFSL3mtXcjsmwDaIrTaPfvOTvG/NzfBmDUugc8RPql2b2LjXJpAzjRY/R+vM1PM+bztz/2jI9Mx+ggF9971wEGayV7NaK0C3ZHZ45TZOPGDWLKj+nyczcbJgNtYnSAeO/ZOj2gV7FRLlT7ozPHKbKxG/3p5wONDdEd/PxTtNszb3bYhah69YssAN2nwnLNwcFpRG2W2+1u9nqDfyY3Oe5CLNRcU5HRuvNup1FuVJwPbwrRUc57ECtbbq0zez2CSxMdZwuxEd32IKoPLoisAJ3jVK7VXQ9voqI6yKHIjOUSpwzfxOLU7yvvXWIUD9G2mzCr3HYykenbdOwbkc+d6qhJz9wYvQzUpJ7bm8j4jUj2J3HL00JXxjNPAzVVHPp9toC2QxssC50Z8fiQVNtMZP5WUptqg5GFZkgz5Rk2HlLZbmTjwztJLdVGruFaOKYcivBrYjlmMdGXN5JOn0iPvpChCpaxG59Xyyluwxmmsgxs+LweEL/O0Guq1/fxLZbmdvgF6XSY8npNp3LJK0inukRer1qVY/wsRJJ5ARq5KN/xS0PzyI3DK4+HiMoDTw/HicjxtdVEAxpqjYY1XACHoxt+vaGmyqVOyOnd6voYVRY48iHCvkbow1jUSQnZ7QqUL4gxH2YTrlI4jmg0qTVOKWioVuCah2qNNyBycYvjuLTB3UFdE3d9fVSl3gmED6nW51FuCv0AInQs/20M0MCBhIa/2ahuBWngQHeif6FaEO+CxtPVUW1vkVKrrHb4dvLOSt7X2TOW6+1gugh7KW3GjIjP96kuoYR2nV0+Fur3s8anKdmpqCz6jooqdrhNkwiVuGvUKYerlUK9MRv100lCpw/PyEq53u+wvqfkg2r3olomd7JSUPud4Lt3TNU6KFwJKCtltb718GjwBlLu7vsF1RuzUkafanTOOV6hYKjk+UW7X1cLiNMKWtHY6vV++6L2OOnGStTuHu4vG32UZ3VVU72OoMV+433n4nwWez2wstmkUkMSBCWZzM7KgDNUqFChQoUKFSpUqFChQoUK9Vj0f0RDrEgIogMPAAAAAElFTkSuQmCC",
        },
        {
            name: 'Yashraj Nagar',
            designation: 'CXO Manager',
            imgUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABQVBMVEX////t5/ZnOrf/yih5VUjttAUxG5J4Rxn/zSfu6PdxTUnt6PnVpjXtvSj8+/7y7vn18vrt3Mzz7v7/yAC8rLFzTT3s6Pz59/x1UUnyuABfLLT/0CnywShiMbVkNbZbJLJxPxhxSThtOxjtugDl3vJZIbLv5OP3wBtTOIJfNbEiC5aBX8J8WMDb0dyXfXni2eaxnqHisTCBXEaIYkV9Wk/SozWFZl3FkyG5iSCpeB6CURpsSUqhcB3tvzPtynWPXxvtx2X22aT+zDqbgs5BJJyvm9fUyOm9rN6JasYaAJgmEJU7I4x0U3OpflGRdclsQbnMv8hqPymehYS7qq6kez+whj2+kjqUbUOfd0D41Yn03bigmMyznqpGNZtnPZJHKnyPhL93arJAH6TRmkurenOZcV7Qw+igiNB8WW1rTHe2pNsRhAU5AAALxElEQVR4nO2de1vbRhbGK7k4EsaWMfh+EQY7EDC3ODg0JIRgDNhtY0IKyTabXnY30PL9P8CO5Itk6zZzZjQyffT+kzzI1uin95wzM5I8+u67UKFChQoVKlSoUKFChQoVKlQoYiV0BX0UrJXIZpOKIsuCWbKsKMlk9tHDJpLKJJhVspJ8rJjZpBecCTOZDfpwCYW8w6Yb6RF5mSAwb9rKRwAJx3sckFny4LSBnN2cTDLAGygZNIqdEizsM6TMWrAy5ps5Rh/4ZovRHz6dMWg0Xezqi52CrzlZX/mE4PsO/wLUUJCh6ruBQwVmIw8DBwrGxgQ3Pk0B2OhvCbWKe1HlF6Ej8Y3UBN0UCSiOQxxeNXRa3JKRdwoa4pSM/FPQEJdkDBKQC2IgNYYnYtCAaCj+Twf0GXEWAH0N1GCLjCHfEGcF0DdEakAZKWUSRcz7gkg1ktHRdrevlieUgu/Qh9ENxVgU4QlXezvparWaNuvDNoWLzMeo8PkuwltGdOnvLUrvUJjIfKYB59u+/mBDp6tKYyLjbhF4JIivZeceExOZVhtglUnt7lQd8TRRZSLLagOrMnJqz8U/BiYyrDag5lO7LXcDB5lI1S2yAgR19amrtIeBA10vU0AySkVQjKY+ehs4CNR0tfVRgDKyiVMQ4B4m4ICyer0LTEgWgJAYJQPUGfdgNjKIU0iMYoeombG1DbKRPk4BZzZ1RQ6IVL2CIFIPbQB9vbwNAkSIHyGItP0+AFCA8YER6QABZSa1g9UP2uoDZMpIVWwAcyZgEo5chAxUaeZR5BbKAtxBTS9kckQKEyEW7tERpvcAcQo3EWDhLk2MaqrucjQRYuE1nYXIxGuOJgZhIV8TiRsitjA3P8/GRBgg+XCGrJDm5nO/LMRtNgjkJsIGNsTNCKllbMLc/Pe/LESKkhTPTW+qXgH6RAggYFKRamHj3exLRSmCJL2ZDlTQxRvIFIP8ROLVmdyRgacj7lsQAb0+YIoB6Sq8gzR39Pxmv2jgaSouHE1+CBSm5B0GYNqU2vHEuzW7N0Y8mEQEjWvIaw15G4LsZmHuqHn7BlWWaTw7RNBVVFJAQJ1xmfnOI7y4ZI+nI95OIkIu2ZDWGsjc3iENdTwH98aIN+ZyAxnWEIcpeQu20wo0aPmk4bnRWRFBpYYwTEFX2KYn97n5o08HJQw8HfGzgZhehhCSVVPIzabJ/l5zDxtPk/RpPLoBFVPCMIWcw9QLM95nIjwdsTlCBA2+CTt9QAOCbBj4+SBCiKerOfo+7KYbCSAkDY2riPMLEDxk4sIoFVu+X/6G3KswE4IAqQlJEhGShiwJX4AISRIRtH+WhLDbifiAoIdnWBKChm0kPSLo0QumhIALGQJJIoJu3M8AIf4lN9DuZ4AQv9RA9u5EKLlMmqY2UhNilxrgU3pyK20lLMZfHr4sFW35iiW0LW5sGxOmW8BHM3BLDfBBS1loWQilH1ZXVlZWD+0Qi4f6th8kC2ELaCH2qAb6rKwsDMbOBqF0uPpE06pNZkoLw22H0iRhrgkFxC6mUMLMcVxHNHm48mSgV1YTi6+G21YmPcw148cZnwmBD3THFtfiOuKYUIqvjijWLYTrI/rVuGQiRIDxtcUY7BBwuwtgjGSOS3Ed0Y6wZCEs2RLmmmgnJaiJuN0FlPANAixFmjlTlI4In1ijVBptWjVFaa4Z0U7TG58JYXsXhLhGKEWaR2PC4uuBUSs2xbR4ONz2ujgmPGpGJI0wDjwCnwmHHkqR5wdG5Xy1uvJkZfWtXacvvdW3vTL+cPAcfZvGQ9wuH0qo5SHKN6m0bwJ6+frta4dRnLSAtr00tkn7KAIiNHmISQj94YFeS/WKYgaSikXHaxrT2/T/lyhqqc+EyMS1uLVmkqoUXwNbiDlsg/94JHayxoBw7QTqoP+EQuYdA8J3YAc5EAoZhRZQUigAORAKmVNKwlMaQC6EJ5SEJzNP+G6JCnCJJgu5EMYWKQnBXSEBIXhcqitDRyhRWejzqG1IeExF+C8uhFS/uqcLU8og9Xl+OBRVf0HXV/AipKimlJUU+yoG7Q/vYVe8dVHFKD4h5RpCcBNpLfT9auJIMWgmUmYhPiHtQlfQckpZSAX8a97Uq+lljiGIS5R9oUBwi5S2IUGGxOkprYMEt7mpF9qJPSU3cekpNSH+/UP6hWgyxKm4tEgdowT3gBksOZf5lQxx6Vd6QIL7+CwW7iRDZAJI8lAUg9aIApVFiApED7YxWdMr8/QUj3Hp9CkTQJJnotis/RgTjrGGqCcCfT+hieS5NkbLd8YW8xiAeeqhzFBEz7KzaTK2ODdnvfk7qfW5OVaEJICMFtfTCN0Z17UPMCIke0aYUSLqhM6M64PNjAjJnvNms5T1iFCDnKZcXx9vY0RI+MsnJm2aCIeYQ03+mREhGSCbMJ0mdBAbQtLfzDDpL7gSEv/GkkU15UlI/hNLBmEqpzgSkv/+kLaayopy/uVjHgMw/+9zxfMFdJ4CLDpA06aG98fXzc3fsAh/3/z6By0kZKkheK2RldoXhBeNRp9hEaIP9ja/fqnRMIKWiwIDyn/qeEj/wQCcmxt8eHPzTwpGCCCw1iAD20M+dNT/9TYxf9sdfrpXvocyAheLAvIVymdjwv9hEH7rjT4ulgttGCMMEHDJTZE7hbIoPhsdchSj1OQ3Rh8+E0WxrHZkQLtAQtIOQ1bu+gV0lCbC7r4n4X7XTCiKhf4dsY3gRYbITqYivK9X9IMUx4TeYZpvjYP02eDLlXqbcNkI+EJRRCYq5/2yONQ4EaNRTw+Nj46+jWw8Jzq5FIt9EbSjPIwMnDTxm7uJ+W/jwntmfL1SvyBpGg6Ib6KstFXRJMPE7r4bYt7IwlGQDqTe4ycj1brXmGdSli8L5uMz1ZrehivhRs/GQj1SL3GTkXKBTzxAoVGePD6TiZt/OSPm/9q0t1DrNxqYiHSAWAMbOWYBNGVitNtyQsy3jBg9s+yhvIWFSL3mtXcjsmwDaIrTaPfvOTvG/NzfBmDUugc8RPql2b2LjXJpAzjRY/R+vM1PM+bztz/2jI9Mx+ggF9971wEGayV7NaK0C3ZHZ45TZOPGDWLKj+nyczcbJgNtYnSAeO/ZOj2gV7FRLlT7ozPHKbKxG/3p5wONDdEd/PxTtNszb3bYhah69YssAN2nwnLNwcFpRG2W2+1u9nqDfyY3Oe5CLNRcU5HRuvNup1FuVJwPbwrRUc57ECtbbq0zez2CSxMdZwuxEd32IKoPLoisAJ3jVK7VXQ9voqI6yKHIjOUSpwzfxOLU7yvvXWIUD9G2mzCr3HYykenbdOwbkc+d6qhJz9wYvQzUpJ7bm8j4jUj2J3HL00JXxjNPAzVVHPp9toC2QxssC50Z8fiQVNtMZP5WUptqg5GFZkgz5Rk2HlLZbmTjwztJLdVGruFaOKYcivBrYjlmMdGXN5JOn0iPvpChCpaxG59Xyyluwxmmsgxs+LweEL/O0Guq1/fxLZbmdvgF6XSY8npNp3LJK0inukRer1qVY/wsRJJ5ARq5KN/xS0PzyI3DK4+HiMoDTw/HicjxtdVEAxpqjYY1XACHoxt+vaGmyqVOyOnd6voYVRY48iHCvkbow1jUSQnZ7QqUL4gxH2YTrlI4jmg0qTVOKWioVuCah2qNNyBycYvjuLTB3UFdE3d9fVSl3gmED6nW51FuCv0AInQs/20M0MCBhIa/2ahuBWngQHeif6FaEO+CxtPVUW1vkVKrrHb4dvLOSt7X2TOW6+1gugh7KW3GjIjP96kuoYR2nV0+Fur3s8anKdmpqCz6jooqdrhNkwiVuGvUKYerlUK9MRv100lCpw/PyEq53u+wvqfkg2r3olomd7JSUPud4Lt3TNU6KFwJKCtltb718GjwBlLu7vsF1RuzUkafanTOOV6hYKjk+UW7X1cLiNMKWtHY6vV++6L2OOnGStTuHu4vG32UZ3VVU72OoMV+433n4nwWez2wstmkUkMSBCWZzM7KgDNUqFChQoUKFSpUqFChQoUK9Vj0f0RDrEgIogMPAAAAAElFTkSuQmCC",
        },
        {
            name: 'Damini Nagar',
            designation: 'Senior Manager',
            imgUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABQVBMVEX////t5/ZnOrf/yih5VUjttAUxG5J4Rxn/zSfu6PdxTUnt6PnVpjXtvSj8+/7y7vn18vrt3Mzz7v7/yAC8rLFzTT3s6Pz59/x1UUnyuABfLLT/0CnywShiMbVkNbZbJLJxPxhxSThtOxjtugDl3vJZIbLv5OP3wBtTOIJfNbEiC5aBX8J8WMDb0dyXfXni2eaxnqHisTCBXEaIYkV9Wk/SozWFZl3FkyG5iSCpeB6CURpsSUqhcB3tvzPtynWPXxvtx2X22aT+zDqbgs5BJJyvm9fUyOm9rN6JasYaAJgmEJU7I4x0U3OpflGRdclsQbnMv8hqPymehYS7qq6kez+whj2+kjqUbUOfd0D41Yn03bigmMyznqpGNZtnPZJHKnyPhL93arJAH6TRmkurenOZcV7Qw+igiNB8WW1rTHe2pNsRhAU5AAALxElEQVR4nO2de1vbRhbGK7k4EsaWMfh+EQY7EDC3ODg0JIRgDNhtY0IKyTabXnY30PL9P8CO5Itk6zZzZjQyffT+kzzI1uin95wzM5I8+u67UKFChQoVKlSoUKFChQoVKlQoYiV0BX0UrJXIZpOKIsuCWbKsKMlk9tHDJpLKJJhVspJ8rJjZpBecCTOZDfpwCYW8w6Yb6RF5mSAwb9rKRwAJx3sckFny4LSBnN2cTDLAGygZNIqdEizsM6TMWrAy5ps5Rh/4ZovRHz6dMWg0Xezqi52CrzlZX/mE4PsO/wLUUJCh6ruBQwVmIw8DBwrGxgQ3Pk0B2OhvCbWKe1HlF6Ej8Y3UBN0UCSiOQxxeNXRa3JKRdwoa4pSM/FPQEJdkDBKQC2IgNYYnYtCAaCj+Twf0GXEWAH0N1GCLjCHfEGcF0DdEakAZKWUSRcz7gkg1ktHRdrevlieUgu/Qh9ENxVgU4QlXezvparWaNuvDNoWLzMeo8PkuwltGdOnvLUrvUJjIfKYB59u+/mBDp6tKYyLjbhF4JIivZeceExOZVhtglUnt7lQd8TRRZSLLagOrMnJqz8U/BiYyrDag5lO7LXcDB5lI1S2yAgR19amrtIeBA10vU0AySkVQjKY+ehs4CNR0tfVRgDKyiVMQ4B4m4ICyer0LTEgWgJAYJQPUGfdgNjKIU0iMYoeombG1DbKRPk4BZzZ1RQ6IVL2CIFIPbQB9vbwNAkSIHyGItP0+AFCA8YER6QABZSa1g9UP2uoDZMpIVWwAcyZgEo5chAxUaeZR5BbKAtxBTS9kckQKEyEW7tERpvcAcQo3EWDhLk2MaqrucjQRYuE1nYXIxGuOJgZhIV8TiRsitjA3P8/GRBgg+XCGrJDm5nO/LMRtNgjkJsIGNsTNCKllbMLc/Pe/LESKkhTPTW+qXgH6RAggYFKRamHj3exLRSmCJL2ZDlTQxRvIFIP8ROLVmdyRgacj7lsQAb0+YIoB6Sq8gzR39Pxmv2jgaSouHE1+CBSm5B0GYNqU2vHEuzW7N0Y8mEQEjWvIaw15G4LsZmHuqHn7BlWWaTw7RNBVVFJAQJ1xmfnOI7y4ZI+nI95OIkIu2ZDWGsjc3iENdTwH98aIN+ZyAxnWEIcpeQu20wo0aPmk4bnRWRFBpYYwTEFX2KYn97n5o08HJQw8HfGzgZhehhCSVVPIzabJ/l5zDxtPk/RpPLoBFVPCMIWcw9QLM95nIjwdsTlCBA2+CTt9QAOCbBj4+SBCiKerOfo+7KYbCSAkDY2riPMLEDxk4sIoFVu+X/6G3KswE4IAqQlJEhGShiwJX4AISRIRtH+WhLDbifiAoIdnWBKChm0kPSLo0QumhIALGQJJIoJu3M8AIf4lN9DuZ4AQv9RA9u5EKLlMmqY2UhNilxrgU3pyK20lLMZfHr4sFW35iiW0LW5sGxOmW8BHM3BLDfBBS1loWQilH1ZXVlZWD+0Qi4f6th8kC2ELaCH2qAb6rKwsDMbOBqF0uPpE06pNZkoLw22H0iRhrgkFxC6mUMLMcVxHNHm48mSgV1YTi6+G21YmPcw148cZnwmBD3THFtfiOuKYUIqvjijWLYTrI/rVuGQiRIDxtcUY7BBwuwtgjGSOS3Ed0Y6wZCEs2RLmmmgnJaiJuN0FlPANAixFmjlTlI4In1ijVBptWjVFaa4Z0U7TG58JYXsXhLhGKEWaR2PC4uuBUSs2xbR4ONz2ujgmPGpGJI0wDjwCnwmHHkqR5wdG5Xy1uvJkZfWtXacvvdW3vTL+cPAcfZvGQ9wuH0qo5SHKN6m0bwJ6+frta4dRnLSAtr00tkn7KAIiNHmISQj94YFeS/WKYgaSikXHaxrT2/T/lyhqqc+EyMS1uLVmkqoUXwNbiDlsg/94JHayxoBw7QTqoP+EQuYdA8J3YAc5EAoZhRZQUigAORAKmVNKwlMaQC6EJ5SEJzNP+G6JCnCJJgu5EMYWKQnBXSEBIXhcqitDRyhRWejzqG1IeExF+C8uhFS/uqcLU8og9Xl+OBRVf0HXV/AipKimlJUU+yoG7Q/vYVe8dVHFKD4h5RpCcBNpLfT9auJIMWgmUmYhPiHtQlfQckpZSAX8a97Uq+lljiGIS5R9oUBwi5S2IUGGxOkprYMEt7mpF9qJPSU3cekpNSH+/UP6hWgyxKm4tEgdowT3gBksOZf5lQxx6Vd6QIL7+CwW7iRDZAJI8lAUg9aIApVFiApED7YxWdMr8/QUj3Hp9CkTQJJnotis/RgTjrGGqCcCfT+hieS5NkbLd8YW8xiAeeqhzFBEz7KzaTK2ODdnvfk7qfW5OVaEJICMFtfTCN0Z17UPMCIke0aYUSLqhM6M64PNjAjJnvNms5T1iFCDnKZcXx9vY0RI+MsnJm2aCIeYQ03+mREhGSCbMJ0mdBAbQtLfzDDpL7gSEv/GkkU15UlI/hNLBmEqpzgSkv/+kLaayopy/uVjHgMw/+9zxfMFdJ4CLDpA06aG98fXzc3fsAh/3/z6By0kZKkheK2RldoXhBeNRp9hEaIP9ja/fqnRMIKWiwIDyn/qeEj/wQCcmxt8eHPzTwpGCCCw1iAD20M+dNT/9TYxf9sdfrpXvocyAheLAvIVymdjwv9hEH7rjT4ulgttGCMMEHDJTZE7hbIoPhsdchSj1OQ3Rh8+E0WxrHZkQLtAQtIOQ1bu+gV0lCbC7r4n4X7XTCiKhf4dsY3gRYbITqYivK9X9IMUx4TeYZpvjYP02eDLlXqbcNkI+EJRRCYq5/2yONQ4EaNRTw+Nj46+jWw8Jzq5FIt9EbSjPIwMnDTxm7uJ+W/jwntmfL1SvyBpGg6Ib6KstFXRJMPE7r4bYt7IwlGQDqTe4ycj1brXmGdSli8L5uMz1ZrehivhRs/GQj1SL3GTkXKBTzxAoVGePD6TiZt/OSPm/9q0t1DrNxqYiHSAWAMbOWYBNGVitNtyQsy3jBg9s+yhvIWFSL3mtXcjsmwDaIrTaPfvOTvG/NzfBmDUugc8RPql2b2LjXJpAzjRY/R+vM1PM+bztz/2jI9Mx+ggF9971wEGayV7NaK0C3ZHZ45TZOPGDWLKj+nyczcbJgNtYnSAeO/ZOj2gV7FRLlT7ozPHKbKxG/3p5wONDdEd/PxTtNszb3bYhah69YssAN2nwnLNwcFpRG2W2+1u9nqDfyY3Oe5CLNRcU5HRuvNup1FuVJwPbwrRUc57ECtbbq0zez2CSxMdZwuxEd32IKoPLoisAJ3jVK7VXQ9voqI6yKHIjOUSpwzfxOLU7yvvXWIUD9G2mzCr3HYykenbdOwbkc+d6qhJz9wYvQzUpJ7bm8j4jUj2J3HL00JXxjNPAzVVHPp9toC2QxssC50Z8fiQVNtMZP5WUptqg5GFZkgz5Rk2HlLZbmTjwztJLdVGruFaOKYcivBrYjlmMdGXN5JOn0iPvpChCpaxG59Xyyluwxmmsgxs+LweEL/O0Guq1/fxLZbmdvgF6XSY8npNp3LJK0inukRer1qVY/wsRJJ5ARq5KN/xS0PzyI3DK4+HiMoDTw/HicjxtdVEAxpqjYY1XACHoxt+vaGmyqVOyOnd6voYVRY48iHCvkbow1jUSQnZ7QqUL4gxH2YTrlI4jmg0qTVOKWioVuCah2qNNyBycYvjuLTB3UFdE3d9fVSl3gmED6nW51FuCv0AInQs/20M0MCBhIa/2ahuBWngQHeif6FaEO+CxtPVUW1vkVKrrHb4dvLOSt7X2TOW6+1gugh7KW3GjIjP96kuoYR2nV0+Fur3s8anKdmpqCz6jooqdrhNkwiVuGvUKYerlUK9MRv100lCpw/PyEq53u+wvqfkg2r3olomd7JSUPud4Lt3TNU6KFwJKCtltb718GjwBlLu7vsF1RuzUkafanTOOV6hYKjk+UW7X1cLiNMKWtHY6vV++6L2OOnGStTuHu4vG32UZ3VVU72OoMV+433n4nwWez2wstmkUkMSBCWZzM7KgDNUqFChQoUKFSpUqFChQoUK9Vj0f0RDrEgIogMPAAAAAElFTkSuQmCC",
        }
    ],
    chartData: {
        topChannelData: {
            type: 'doughnut',
            data: {
                labels: ["2015", "2016", "2017", "2018", "2019"],
                datasets: [{
                    data: [25, 10, 20, 25, 20],
                    backgroundColor: [
                        '#ffba1f',
                        '#3bc470',
                        '#8499f2',
                        '#34c9be',
                        '#aa7bd8'
                    ],
                    
                }]
            },
            borderWidth:0,
            options: {
                cutoutPercentage:80,
                legend: {
                    labels: {
                        fontColor: "white",
                        fontSize: 12
                    },
                    display: false
                },
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        labels: {
                            color: 'rgb(255, 255, 255)'
                        }
                    }
                },
                elements: {
                    arc: {
                        borderWidth: 0
                    },
                    center: {
                        text: 'Red is 2/3 the total numbers',
                        color: '#FF6384', // Default is #000000
                        fontStyle: 'Arial', // Default is Arial
                        sidePadding: 20, // Default is 20 (as a percentage)
                        minFontSize: 20, // Default is 20 (in px), set to false and text will not wrap.
                        lineHeight: 25 // Default is 25 (in px), used for when text wraps
                      }
                }
            },
            centerText: {
                display: true,
                text: "280"
            },
            tooltips: {
                position: 'average',
                caretSize: 5,
                xPadding: 25,
                yPadding: 10,
                titleFontColor: 'rgba(173, 0, 87, 1)',
                titleFontStyle: 'bold',
                titleFontSize: 14,
                backgroundColor: '#F0F0F0',
                cornerRadius: 10,
                callbacks: {
                    labelColor: function () {
                        return {
                            borderColor: 'rgb(0, 0, 255)',
                            backgroundColor: 'rgb(255, 0, 0)',
                        };
                    },
                    labelTextColor: function () {
                        return 'rgba(173, 0, 87, 1)';
                    },
                    // use label callback to return the desired label
                    label: function () {
                        return null
                    },
                    title: function (tooltipItem: any, data: any) {
                        // debugger
                        // console.log(data)
                        return tooltipItem[0].label + " | " + tooltipItem[0].value;
                    }
                },
            }
        },
        topLandingPage: {
            type: 'doughnut',
            data: {
                labels: ["2015", "2016", "2017", "2018", "2019"],
                datasets: [{
                    // label: '# of Tomatoes',
                    data: [45, 79, 100, 65, 68],
                    backgroundColor: [
                        '#ffdd4d',
                        '#ffdd4d',
                        '#ffdd4d',
                        '#ffdd4d',
                        '#ffdd4d'
                    ],
                    barPercentage: 0.2,
                    radius: 12
                }]
            },
            options: {
                legend: {
                    labels: {
                        fontColor: "white",
                        fontSize: 12
                    },
                    display: false
                },
                responsive: true,
                scales: {
                    xAxes: [
                        {
                            position: "bottom",
                            ticks: {
                                fontColor: 'rgba(255,255,255,0.5)'
                            },
                            gridLines: {
                                display: false,
                            },
                        }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontColor: 'rgba(255,255,255,0.5)',
                            stepSize: 20
                        },
                        scaleLabel: {
                            display: true,
                            // labelString: 'Percentage',
                        },
                        gridLines: {
                            borderDashOffset: 3,
                            borderDash: [5, 2],
                            color: 'rgba(255,255,255,0.3)'
                        },
                    }]
                },
                plugins: {
                    legend: {
                        display: true,
                        labels: {
                            color: 'rgb(255, 255, 255)'
                        }
                    }
                },
            },
        },
        topContentData: {
            type: 'bar',
            data: {
                labels: ["2015", "2016", "2017", "2018", "2019"],
                datasets: [{
                    // label: '# of Tomatoes',
                    data: [45, 79, 100, 65, 68],
                    backgroundColor: [
                        '#ffdd4d',
                        '#ffdd4d',
                        '#ffdd4d',
                        '#ffdd4d',
                        '#ffdd4d'
                    ],
                    barPercentage: 0.1  ,
                    radius: 12
                }]
            },
            options: {
                legend: {
                    labels: {
                        fontColor: "white",
                        fontSize: 12
                    },
                    display: false
                },
                responsive: true,
                scales: {
                    xAxes: [
                        {
                            position: "bottom",
                            ticks: {
                                fontColor: 'rgba(255,255,255,0.5)'
                            },
                            gridLines: {
                                display: false,
                            },
                        }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontColor: 'rgba(255,255,255,0.5)',
                            stepSize: 20
                        },
                        scaleLabel: {
                            display: true,
                            // labelString: 'Percentage',
                        },
                        gridLines: {
                            borderDashOffset: 3,
                            borderDash: [5, 2],
                            color: 'rgba(255,255,255,0.3)'
                        },
                    }]
                },
                plugins: {
                    legend: {
                        display: true,
                        labels: {
                            color: 'rgb(255, 255, 255)'
                        }
                    }
                },
            },
        },
        dayWiseStats:{
            type: 'line',
            data: {
                labels: ["S", "M", "T", "W", "T", "F", "S"],
                datasets: [{
                    // label: '# of Tomatoes',
                    data: [45, 79, 100, 65, 68, 74, 42],
                    backgroundColor:'rgba(83, 122, 175, 0.2)',
                    borderColor: '#00BFFF',
                }]
            },
            options: {
                legend: {
                    labels: {
                        fontColor: "white",
                        fontSize: 12
                    },
                    display: false
                },
                responsive: true,
                scales: {
                    xAxes: [
                        {
                            position: "bottom",
                            ticks: {
                                fontColor: 'rgba(255,255,255,0.5)'
                            },
                            gridLines: {
                                display: true,
                                color: 'rgba(255,255,255,0.3)',
                            },
                        }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontColor: 'rgba(255,255,255,0.5)',
                            stepSize: 10
                        },
                        scaleLabel: {
                            display: false,
                        },
                        gridLines: {
                            display: false,
                            color: 'rgba(255,255,255,0.3)',
                        },
                    }]
                },
                plugins: {
                    legend: {
                        display: true,
                        labels: {
                            color: 'rgb(255, 255, 255)'
                        }
                    }
                },
            },
        },
        timeWiseStats:{
            type: 'bar',
            data: {
                labels: ["2015", "2016", "2017", "2018", "2019"],
                datasets: [{
                    // label: '# of Tomatoes',
                    data: [45, 79, 100, 65, 68],
                    backgroundColor: '#00BFFF',
                    barPercentage: 0.1,
                    radius: 12
                }]
            },
            options: {
                legend: {
                    labels: {
                        fontColor: "white",
                        fontSize: 12
                    },
                    display: false
                },
                responsive: true,
                scales: {
                    xAxes: [
                        {
                            position: "bottom",
                            ticks: {
                                fontColor: 'rgba(255,255,255,0.5)'
                            },
                            gridLines: {
                                display: false,
                            },
                        }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontColor: 'rgba(255,255,255,0.5)',
                            stepSize: 20
                        },
                        scaleLabel: {
                            display: true,
                            // labelString: 'Percentage',
                        },
                        gridLines: {
                            borderDashOffset: 3,
                            borderDash: [5, 2],
                            color: 'rgba(255,255,255,0.3)'
                        },
                    }]
                },
                plugins: {
                    legend: {
                        display: true,
                        labels: {
                            color: 'rgb(255, 255, 255)'
                        }
                    }
                },
            },
        }
    }
}