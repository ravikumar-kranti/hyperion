import { Component, OnInit } from '@angular/core';

import { campaignList } from "../Data/campaignList";
import { CampaignListModel } from "./../models/campaignList.model";
import { projectTeamList } from '../Data/projectTeam';
import { projectTeamModel } from '../models/projectTeam.model';
import { DashboardData } from "./../Data/dashboard";

import Chart from 'chart.js';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	campaignLists: CampaignListModel[] = campaignList;
	projectTeamLists: projectTeamModel[] = projectTeamList;
	dashboardData = DashboardData;
	/* Top Channel Canvas */
	channelCanvas: any;
	channelLabels = ['SMS', 'Email', 'IVR', 'W.App', 'Facebook'];
	/* Top Landing page canvas */
	landingPageCanvas: any;
	channelColors = ['#5dd4c9', '#0eaa45', '#42e7a1', '#ace742', '#2bd366'];
	values = [47, 53, 23, 12, 34];
	labels = ['A', 'B', 'C', 'D', 'E'];

	/* Daywise */
	daywiseCanvas: any;
	timewiseCanvas: any;

	ngOnInit() { }

	constructor() { }

	ngAfterViewInit() {
		this.channelDoughnet();
		this.LandingPageDoughnut(125, 125, 100, 20, this.values, this.channelColors, this.labels, 1);
		this.contentChart();
		this.daywiseChart();
		this.timewiseChart();
	}

	landingPageDrDwn: boolean = false;
	landingPageDropDown() {
		this.landingPageDrDwn = !this.landingPageDrDwn;
		this.timewiseStatsDrDwn = false;
		this.daywiseStatsDrDwn = false;
	}

	timewiseStatsDrDwn: boolean = false;
	timewiseStatsDropDown() {
		this.timewiseStatsDrDwn = !this.timewiseStatsDrDwn;
		this.landingPageDrDwn = false;
		this.daywiseStatsDrDwn = false;
	}

	daywiseStatsDrDwn: boolean = false;
	daywiseStatsDropDown() {
		this.daywiseStatsDrDwn = !this.daywiseStatsDrDwn;
		this.landingPageDrDwn = false;
		this.timewiseStatsDrDwn = false;
	}

	channel_ctx: any;
	content_ctx: any;
	landing_page_ctx: any;
	daywise_ctx: any;
	timewise_ctx: any;
	topChannelCanvas: any;
	channelDoughnet() {
		this.channelCanvas = document.getElementById("topChannelCanvas");
		this.channel_ctx = this.channelCanvas.getContext("2d");
		// var myChart = new Chart(this.channel_ctx, this.dashboardData.chartData.topChannelData);
		var myChart = new Chart(this.channel_ctx, {
			type: 'doughnut',
			data: {
				labels: ["2015", "2016", "2017", "2018", "2019"],
				datasets: [{
					data: [25, 10, 20, 25, 20],
					backgroundColor: [
						'#ffba1f',
						'#3bc470',
						'#8499f2',
						'#34c9be',
						'#aa7bd8'
					],

				}]
			},
			// borderWidth:0,
			options: {

				cutoutPercentage: 80,
				legend: {
					labels: {
						fontColor: "white",
						fontSize: 12,
					},
					display: false,
				},
				responsive: true,

				plugins: {
					legend: {
						display: true,
						labels: {
							color: 'rgb(255, 255, 255)',
						}
					},

					// labels: {
					// 	render: 'label',
					// 	fontColor: '#000',
					// 	position: 'outside'
					// },
				},
				elements: {
					arc: {
						borderWidth: 0
					},
				},
				tooltips: {
					enabled: false,
					position: 'nearest',
					bodyAlign: 'right',
					bodySpacing: 5,
					caretSize: 5,
					xPadding: 25,
					yPadding: 10,
					titleFontColor: 'red',
					titleFontStyle: 'bold',
					titleFontSize: 14,
					backgroundColor: '#F0F0F0',
					cornerRadius: 10,
					callbacks: {
						labelColor: function () {
							return {
								borderColor: 'rgb(0, 0, 255)',
								backgroundColor: 'rgb(255, 0, 0)',
							};
						},
						labelTextColor: function () {
							return 'rgba(173, 0, 87, 1)';
						},
					},
				},
			},
		});
		/* Subtitle */
		console.log(myChart);
		console.log(this.channel_ctx);
		myChart.ctx?.fillText('75' + "%", 100 / 2 - 20, 100 / 2, 200);
		this.channel_ctx.fillStyle = "black";
	}

	/* Top Landing Page Chart */
	LandingPageDoughnut(cx: any, cy: any, radius: any, arcwidth: any, values: any, colors: any, labels: any, selectedValue: any) {
		this.landingPageCanvas = document.getElementById("landingPageCanvas");
		var ctx = this.landingPageCanvas.getContext("2d");
		var tot = 0;
		var accum = 0;
		var PI = Math.PI;
		var PI2 = PI * 2;
		var offset = -PI / 2;
		ctx.lineWidth = arcwidth;
		for (var i = 0; i < values.length; i++) { tot += values[i]; }
		for (var i = 0; i < values.length; i++) {
			ctx.beginPath();
			ctx.arc(cx, cy, radius,
				offset + PI2 * (accum / tot),
				offset + PI2 * ((accum + values[i]) / tot)
			);
			ctx.strokeStyle = colors[i];
			ctx.stroke();
			accum += values[i];
		}
		var innerRadius = 85;
		ctx.beginPath();
		ctx.arc(cx, cy, innerRadius, 0, PI2);
		ctx.fillStyle = 'rgba(83, 122, 175, 0.8)'; //Filling inside doughnut
		ctx.fill();
		ctx.textAlign = 'center';
		ctx.textBaseline = 'bottom';
		ctx.fillStyle = 'white';
		/* Title */
		ctx.font = (10) + 'px verdana';
		ctx.fillText('Total', cx, cy + innerRadius * .2);
		/* Subtitle */
		ctx.font = (20) + 'px verdana';
		ctx.fillText('678', cx, cy - innerRadius * 0);
	}

	contentCanvas: any;
	contentChart() {
		this.contentCanvas = document.getElementById('contentCanvas');
		this.content_ctx = this.contentCanvas.getContext('2d');
		var myChart = new Chart(this.content_ctx, this.dashboardData.chartData.topContentData);
	}

	daywiseChart() {
		this.daywiseCanvas = document.getElementById('DaywiseCanvas');
		this.daywise_ctx = this.daywiseCanvas.getContext('2d');
		var myChart = new Chart(this.daywise_ctx, this.dashboardData.chartData.dayWiseStats);
	}

	timewiseChart() {
		this.timewiseCanvas = document.getElementById('TimewiseCanvas');
		this.timewise_ctx = this.timewiseCanvas.getContext('2d');
		var myChart = new Chart(this.timewise_ctx, this.dashboardData.chartData.timeWiseStats);
	}
}
