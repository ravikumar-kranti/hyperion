import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'sidebar',
	templateUrl: './sidebar-submenu.component.html',
	styleUrls: ['./sidebar-submenu.component.scss']
})
export class SidebarSubmenuComponent implements OnInit {
	status: boolean = false;
	segmentSubMenuVisible: boolean = false;
	projectsSubMenuVisible: boolean = false;
	contentLibrarySubMenuVisible: boolean = false;

	constructor() { }

	ngOnInit(): void {
	}
	toggleSidebar() {
		this.status = !this.status;
	}
	dropdownEvent(event: any) {
		console.log(event.target.parentElement.innerText)
		if (event.target.parentElement.innerText === "Projects"){
			this.projectsSubMenuVisible = !this.projectsSubMenuVisible
		}
		if (event.target.parentElement.innerText === "Segments"){
			this.segmentSubMenuVisible = !this.segmentSubMenuVisible
		}
		if (event.target.parentElement.innerText === "Content Library"){
			this.contentLibrarySubMenuVisible = !this.contentLibrarySubMenuVisible
		}
	}
}

