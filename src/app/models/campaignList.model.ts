export interface CampaignListModel {
	imageUrl: string,
	imageSrcset: string,
	campaignName: string,
	campaignZone: string,
	campaignRecords: string,
	campaignDate: string,
	campaignEngagement: string
}