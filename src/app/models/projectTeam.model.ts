export interface projectTeamModel {
    imgUrl: string,
    name: string,
    designation: string
}